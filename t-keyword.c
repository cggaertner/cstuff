/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "keyword.h"

#undef NDEBUG
#include <assert.h>

int main(void)
{
	{
		static const unsigned long long FOO = KW3('f', 'o', 'o');
		assert(FOO == keyword("foo"));
	}

	return 0;
}
