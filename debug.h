#ifndef DEBUG_H_
#define DEBUG_H_

#include <stdarg.h>
#include <stdio.h>
#include <stdlib.h>

#define die(...) (die_(__VA_ARGS__, 0), 0)
#define die_(FORMAT, ...) (die)(FORMAT "\n", __VA_ARGS__)

#define panic(...) (panic_(__VA_ARGS__, 0), 0)
#define panic_(FORMAT, ...) (panic)("PANIC in %s, line %i: " FORMAT "\n", \
	__FILE__, __LINE__, __VA_ARGS__)

static inline void (die)(const char *format, ...)
{
	va_list args;
	va_start(args, format);
	vfprintf(stderr, format, args);
	exit(EXIT_FAILURE);
	va_end(args);
}

static inline void (panic)(const char *format, ...)
{
	va_list args;
	va_start(args, format);
	vfprintf(stderr, format, args);
	abort();
	va_end(args);
}

#endif
