/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "closure.h"

struct closure
{
	void (*const tram)(void);
	void (*func)(void *);
	void *arg;
};

static struct closure closures[8];
static struct closure *next = closures;

closure_t closure_create(int *id, void func(void *), const void *arg)
{
	if(!(next < (&closures)[1]))
		return 0;

	struct closure *clo = next;
	next = clo->arg ? clo->arg : clo + 1;

	clo->func = func;
	clo->arg = (void *)arg;

	if(id) *id = (int)(clo - closures);
	return clo->tram;
}

void closure_destroy(int id)
{
	closures[id].func = 0;
	closures[id].arg = next;
	next = closures + id;
}

static void tram_0(void) { closures[0].func(closures[0].arg); }
static void tram_1(void) { closures[1].func(closures[1].arg); }
static void tram_2(void) { closures[2].func(closures[2].arg); }
static void tram_3(void) { closures[3].func(closures[3].arg); }
static void tram_4(void) { closures[4].func(closures[4].arg); }
static void tram_5(void) { closures[5].func(closures[5].arg); }
static void tram_6(void) { closures[6].func(closures[6].arg); }
static void tram_7(void) { closures[7].func(closures[7].arg); }

static struct closure closures[] = {
	{ tram_0, 0, 0 },
	{ tram_1, 0, 0 },
	{ tram_2, 0, 0 },
	{ tram_3, 0, 0 },
	{ tram_4, 0, 0 },
	{ tram_5, 0, 0 },
	{ tram_6, 0, 0 },
	{ tram_7, 0, 0 },
};
