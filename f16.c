/*	Copyright 2013 Christoph Gärtner
	Distributed under the Boost Software License, Version 1.0
*/

#include "f16.h"
#if 0
static inline uint64_t to_bits(double d)
{
	union { double value; uint64_t bits; } db = { d };
	return db.bits;
}
#endif
static inline double from_bits(uint64_t u)
{
	union { uint64_t bits; double value; } db = { u };
	return db.value;
}

double f16_dec(uint16_t h)
{
	uint64_t sign = (uint64_t)(h & 0x8000) << 48;
	uint64_t exp = ((uint64_t)(h & 0x7C00) << 42) + 0x3F00000000000000;
	uint64_t frac = (uint64_t)(h & 0x03FF) << 42;

	// zero or subnormal
	if(exp == 0x3F00000000000000)
	{
		if(frac == 0)
			return from_bits(sign);

		while((frac & (uint64_t)1 << 51) == 0)
			frac <<= 1, exp -= (uint64_t)1 << 52;

		frac &= ~((uint64_t)1 << 51);
		frac <<= 1;

		return from_bits(sign | exp | frac);
	}

	// infinity of NaN
	if(exp == 0x40F0000000000000)
		return from_bits(sign | 0x7FF0000000000000 | frac);

	return from_bits(sign | exp | frac);
}

uint16_t f16_enc(double d)
{
	(void)d;
	// TODO
	return 0;
}
